//Realizar una expresión regular que detecte emails correctos.

@"^[^@\s]+@[^@\s]+\.[^@\s]+$"


//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.

/^([A-Z]{4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM](AS|BC|BS|CC|CL|CM|CS|CH|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9A-Z]\d)$/i



//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.

'/^((.){1,50}(\s+(.){1.50})*)$/i'


//Crea una funcion para escapar los simbolos especiales.

<?php
	$specials = '.\+*?[^]$(){}=!<>|:-';
	$clave='test/+--:'
	preg_quote($clave,$specials);
?>


//Crear una expresion regular para detectar números decimales.

[0-9]+(\.[0-9][0-9]?)?