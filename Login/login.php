<html>
<head>
    <title>Iniciar Sesion</title>
    <meta charset="UTF-8">

    <style type="text/css">


    body {
		font-family: Arial;
		background-color: #3498DB;
		padding: 50px;
		}
	.login {
		text-align: center;
		background-color: #ECF0F1;
		border: 2px solid transparent;
		border-radius: 3px;
		font-size: 16px;
		font-weight: 200;
		padding: 10px 0;
		width: 300px;
		transition: border .5s;
		}
	.Form-login{
		text-align: center;
		width: 400px;
		height: 250px;
		background-color: white;
		border-radius: 3px;
		padding: 20px 20px;
		border-top: 3px solid #000000;
		margin: 20px auto;
	}

	.Form-login h1 {
		font-family: Arial;
		color: #636363;
		font-weight: 700;
	
	}
	.boton{
		border: 2px solid transparent;
  		background: #3498DB;
  		color: #ffffff;
  		font-size: 16px;
  		line-height: 25px;
  		padding: 10px 0;
  		text-decoration: none;
  		text-shadow: none;
  		border-radius: 3px;
  		box-shadow: none;
  		transition: 0.25s;
  		display: block;
  		width: 300px;
  		margin: 0 auto;
	}
	.boton:hover {
  		background-color: #2980B9;
	}



    </style>

</head>


<body>
	<form class="Form-login" action="procesar_formulario.php" method="POST">
		<h1>Login</h1>
		<input  class="login" type="text" name="Numero_de_cuenta" placeholder="Numero_de_cuenta" required />
		<input class="login" type="password" name="Contraseña" placeholder="Contraseña" required />
		<input class="boton" type="submit" name="Enviar" value="Entrar"/>


	</form>
</body>
</html>