<?php
session_start();
if(isset($_SESSION['Alumno'])){
	$usuarioactivo=$_SESSION['Alumno'];

}
else{
	header('location: login.php');
}

if(isset($_POST['botoncerrar'])){
	session_destroy();
	header('location: login.php');
}


if (!empty($_POST)) {

	$alert='';
	if(empty($_POST['Numero_cta'])||empty($_POST['nombre'])||empty($_POST['pri_apell'])||empty($_POST['seg_apell'])||empty($_POST['genero'])||empty($_POST['fecha'])||empty($_POST['Contraseña'])){

		$alert='<p class="alert">Todos los campos son obligatorios</p>';
	}	
	else{

		$dbhost="localhost";
		$dbuser="root";
		$dbpass="";
		$dbname="logintest";

		$conn=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

		$Numero_cta=$_POST['Numero_cta'];
		$nombre=$_POST['nombre'];
		$pri_apell=$_POST['pri_apell'];
		$seg_apell=$_POST['seg_apell'];
		$genero=$_POST['genero'];
		$fecha=$_POST['fecha'];
		$Contraseña=$_POST['Contraseña'];

		$query=mysqli_query($conn,"Select * from login where usuario = '$nombre'");
		$result=mysqli_fetch_array($query);

		if ($result>0) {
			$alert='<p class="alert">El correo o usuario ya existen</p>';	
		}
		else{
			$query_insert=mysqli_query($conn,"Insert into login(usuario,password,num_cta,primer_apellido,segundo_apellido,genero,fecha_nac)
				values('$nombre','$Contraseña','$Numero_cta','$pri_apell','$seg_apell','$genero','$fecha')
				");
			if($query_insert){
				$alert='<p class="alert">Usuario creado correcatamente</p>';
			}
			else{
				$alert='<p class="alert">Error al crear el usuario</p>';
			}
		
		}
	} 

} 
?>


<html>
<head>
	<title>Registrar usuarios</title>
	<meta charset="UTF-8">
	<style type="text/css">
		
		body {
		margin: 0;
		padding: 0;
	    font-family: sans-serif;
		}

		ul{
			text-align: center;
			margin: 20 px auto;
			width: auto;
		}

		a {
			text-decoration: none;
		}
		nav {
			background: #0ca0d6;
			font-size: 10px;
			position: relative;
		    
		}

		nav > ul > li {
			display: inline-block;
		  	font-size: 20px;
		  	padding: 0 15px;
		  	position: relative;
		}

		nav > ul > li > a {
			color: #fff;
		  	display: block;
		  	padding: 20px 0;
		    transition: all .3s ease;
		}
		nav > ul > li:hover > a {
			color: #2E4053; 
		  	background-color: 3px solid #2E4053;
		}
		.boton{
			background: #0ca0d6;
			font-size: 20px;
			color: #fff;
		  	display: block;
		  	padding: 20px 0;
  			margin: 20px auto;
  			border: none;
		}
		.boton:hover {
  			color: #2E4053;
		}

		.form-label{
			display:block;
			line-height:1.2rem;
			padding:.3rem 0
		}
		.form-label.label-sm{
			font-size:.7rem;
			padding:.1rem 0
		}
		.form-label.label-lg{
			font-size:.9rem;
			padding:.4rem 0
		}
		.form-input{
			appearance:none;
			background:#fff;
			background-image:none;
			border:.05rem solid #bcc3ce;
			border-radius:.1rem;
			color:#3b4351;
			display:block;
			font-size:.8rem;
			height:1.8rem;
			line-height:1.2rem;
			max-width:100%;
			outline:0;
			padding:.25rem .4rem;
			position:relative;
			transition:background .2s,border .2s,box-shadow .2s,color .2s;
			width:100%
		}
		.alert{
			text-align: center;
			color: white;
			width: 300px;
			background: #56A8FF;
			border-radius: 6px;
			margin: 20px auto;
		}




	</style>

</head>

<body>
	<nav>
	  <ul>
	    <li>
	    	<a href='info.php'>Home</a>
	    </li>

	    <li>
	      	<a href='registro.php'>Registrar Alumnos </a>
	    </li>

	    <li>
	    	<form method="POST">
	    		<input class="boton" type="submit" value="Cerrar Sesion" name="botoncerrar">
	    </form>
	    </li>

	  </ul>
	</nav>	

	<section class="container2">
		<div>
			<h2>Registro de usuarios</h2>
			<div class="form_registrer">
				<hr>
				<div class="alert">
					<?php
					echo isset($alert) ? $alert : '';
					?>
				</div>
				
				<form action="" method="POST">
					<label class="form-label" for="Numero_cta">Numero de cuenta</label>
					<input class="form-input " type="text" name="Numero_cta" id="Numero_cta" placeholder="Numero de cuenta">

					<label class="form-label" for="nombre">Nombre</label>
					<input class="form-input " type="text" name="nombre" id="nombre" placeholder="Nombre">

					<label class="form-label" for="pri_apell">Primer Apellido</label>
					<input class="form-input " type="text" name="pri_apell" id="pri_apell" placeholder="Primer Apellido">

					<label class="form-label" for="seg_apell">Segundo Apellido</label>
					<input class="form-input " type="text" name="seg_apell" id="seg_apell" placeholder="Segundo Apellido">

					<label class="form-label" for="genero">Genero</label>
					<label class="form-radio">
						<input type="radio" name="genero" value="H" checked>
						<i class="form-icon"></i>Hombre
					</label>
					<label class="form-radio">
						<input type="radio" name="genero" value="M" checked>
						<i class="form-icon"></i>Mujer
					</label>
					<label class="form-radio">
						<input type="radio" name="genero" value="O" checked>
						<i class="form-icon"></i>Otro
					</label>

					<label class="form-label" for="fecha">Fecha de nacimiento</label>
					<input class="form-input " type="text" name="fecha" id="fecha" placeholder="aaaa/mm/dd">

					<label class="form-label" for="Contraseña">Contraseña</label>
					<input class="form-input " type="password" name="Contraseña" id="Contraseña" placeholder="Contraseña">
					</br>

					<input type="submit" value="Registrar" class="botonregis">


				</form>
			</div>
		</div>
	</section>
</body>
</html>