<?php

class Carro{
	//declaracion de propiedades
	public $color;
	
}


//crea aqui la clase Moto junto con dos propiedades public
class moto{

    public $tipo;
    public $tamaño;
    public $marca;


}

//inicializamos el mensaje que lanzara el servidor con vacio
$mensajeServidor0='';
$mensajeServidor1='';
$mensajeServidor2='';
$mensajeServidor3='';


//crea aqui la instancia o el objeto de la clase Moto


$Carro1 = new Carro;
$moto1= new moto;

 if ( !empty($_POST)){

 	//almacenamos el valor mandado por POST en el atributo color
 	$Carro1->color=$_POST['color'];
 	$moto1->tipo=$_POST['tipo'];
    $moto1->tamaño=$_POST['tamaño'];
    $moto1->marca=$_POST['marca'];


 	//se construye el mensaje que sera lanzado por el servidor
 	$mensajeServidor0='el servidor dice que ya escogiste un color: '.$_POST['color'];
 	$mensajeServidor1='El servidor dice que ya ingresaste un tipo: '.$_POST['tipo'];
    $mensajeServidor2='El servidor dice que ya ingresaste un tamaño: '.$_POST['tamaño'];
    $mensajeServidor3='El servidor dice que ya ingresaste un marca: '.$_POST['marca'];


 	 // recibe aqui los valores mandados por post 
 }  

?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor0; ?>" readonly>

	<!-- aqui puedes insertar el mesaje del servidor para Moto-->
	<?php  
        if ( !empty($_POST)){

            echo $mensajeServidor1;
            echo "<br>";
            echo $mensajeServidor2; 
            echo "<br>";
            echo $mensajeServidor3;  

        }

    ?>

	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Carro y Moto</h1></header><br>
	<form method="post">
		<div class="form-group row">
			
			 <label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
			 <div class="col-sm-10">
					<input class="form-control" type="color" name="color" id="CajaTexto1">
			</div>
						<!-- inserta aqui los inputs para recibir los atributos del objeto-->
			<label class="col-sm-3" for="prim1">Tipo</label>
			<input class="form-control" type="text" name="tipo" id="prim1" placeholder="Ingresa un tipo">
            
     
            <label class="col-sm-3" for="prim2">Tamaño</label>
            <input class="form-control" type="text" name="tamaño" id="prim2" placeholder="Ingresa un tamaño">
            
        
            <label class="col-sm-3" for="prim3">Marca</label>
            <input class="form-control" type="text" name="marca" id="prim3" placeholder="Ingresa una marca">
            

		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>

	</div>


</body>
</html>

