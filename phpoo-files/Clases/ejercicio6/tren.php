<?php
include_once('transporte.php');
	class tren extends transporte{

		private $Tipo;
		
		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$tip){
			parent::__construct($nom,$vel,$com);
			$this->Tipo=$tip;
		}

		// sobreescritura de metodo
		public function resumenTren(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Tipo: </td>
						<td>'. $this->Tipo.'</td>				
					</tr>';
			return $mensaje;
		}
	}
?>