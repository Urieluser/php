<?php  
include('carro.php');
include('avion.php');
include('barco.php');
include('tren.php');

	//declaracion de la clase hijo o subclase Carro

$mensaje='';


if (!empty($_POST)){
	//declaracion de un operador switch
	switch ($_POST['tipo_transporte']) {
		case 'aereo':
			//creacion del objeto con sus respectivos parametros para el constructor
			$jet1= new avion('jet','400','gasoleo','2');
			$mensaje=$jet1->resumenAvion();
			break;
		case 'terrestre':
			$carro1= new carro('carro','200','gasolina','4');
			$mensaje=$carro1->resumenCarro();
			break;
		case 'maritimo':
			$bergantin1= new barco('bergantin','40','na','15');
			$mensaje=$bergantin1->resumenBarco();
			break;	
		case 'ferreo':
			$tren1= new tren('Suburbano','200','Electrico','Comercial');
			$mensaje=$tren1->resumenTren();
			break;
	}

}

?>
