<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $año;
	public $veri;

	//declaracion del método verificación
	public function verificacion(){
		if ($this->año<"1990-01") {
			return $this->veri='No circula';
		}
		elseif ($this->año>="1990-01" && $this->año<="2010-01") {
			$this->veri='Realizar revision';
		}
		elseif ($this->año>"2010-01") {
			$this->veri='Si circula';
		}
		else{
			$this->veri='El año no es valido';
		}




	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->año=$_POST['año'];
}




