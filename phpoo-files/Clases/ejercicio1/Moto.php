<?php

//crea aqui la clase Moto junto con dos propiedades public
class moto{

    public $tipo;
    public $tamaño;
    public $marca;


}


//crea aqui la instancia o el objeto de la clase Moto
$moto1= new moto;

 if ( !empty($_POST)){

 	 // recibe aqui los valores mandados por post y arma el mensaje para front 

    $moto1->tipo=$_POST['tipo'];
    $moto1->tamaño=$_POST['tamaño'];
    $moto1->marca=$_POST['marca'];

    //se construye el mensaje que sera lanzado por el servidor

    $mensajeServidor1='El servidor dice que ya ingresaste un tipo: '.$_POST['tipo'];
    $mensajeServidor2='El servidor dice que ya ingresaste un tamaño: '.$_POST['tamaño'];
    $mensajeServidor3='El servidor dice que ya ingresaste un marca: '.$_POST['marca'];
 }  

?>

<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <title>
        Moto
    </title>
</head>
<body>
    
    <?php  
        if ( !empty($_POST)){
            echo $mensajeServidor1;
            echo "<br>";
            echo $mensajeServidor2; 
            echo "<br>";
            echo $mensajeServidor3;  

        }

    ?>

    <!-- aqui puedes insertar el mesaje del servidor para Moto-->


    <div class="container" style="margin-top: 4em">
    
    <header> <h1>Moto</h1></header><br>
    <form method="post">

            <!-- inserta aqui los inputs para recibir los atributos del objeto-->
            <input type="text" name="tipo" id="prim1" placeholder="Ingresa un tipo">
            <label for="prim1">Tipo</label>
            <br>
            <input type="text" name="tamaño" id="prim2" placeholder="Ingresa un tamaño">
            <label for="prim2">Tamaño</label>
            <br>
            <input type="text" name="marca" id="prim3" placeholder="Ingresa una marca">
            <label for="prim3">Marca</label>
            <br>

                        
        </div>
        <button class="btn btn-primary" type="submit" >enviar</button>
        <a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
    </form>

    </div>


</body>
</html>


