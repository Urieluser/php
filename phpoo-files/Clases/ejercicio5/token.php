<?php  
//declaracion de clase token
	class token{
		//declaracion de atributos
		private $nombre;
		private $token;
		//declaracion del metodo para obetener la contraseña aleatoria
		public function rand_chars($n) {
		    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $randomString = '';
		  
		    for ($i = 0; $i < $n; $i++) {
		        $index = rand(0, strlen($characters) - 1);
		        $randomString .= $characters[$index];
		    }
		  
		    return $randomString;
		}


		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
			$this->token=self::rand_chars(4);
		}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
		public function mostrar(){
			return 'Hola '.$this->nombre.' Esta es tu contraseña: '.$this->token;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			//destruye token
			$this->token='La contraseña ha sido destruida';
			echo $this->token;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$token1= new token($_POST['nombre']);
	$mensaje=$token1->mostrar();
}


?>
